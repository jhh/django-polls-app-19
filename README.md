# django-polls introduction app for django 1.9 #

This repository have no practical use, it was only sat up for learning more about packaging and distribution via git repository.

This repository contains code from following the Django introduction at https://docs.djangoproject.com/en/1.9/intro and a "requirements.txt" file that make it possible to install the introduction app as a package.

## Initial steps for installing package ##

No need to download this repository, only requirements.txt file is needed. Below is steps installing the django-polls app as a package with Django. Note that the steps expects [pyenv](https://github.com/yyuu/pyenv) and [pyenv-virtualenv](https://github.com/yyuu/pyenv-virtualenv) for handling Python versioning and virtualenv.

```
$ mkdir some/dir/django-polls-test  
$ cd some/dir/django-polls-test  
$ pyenv install 3.5.2
$ pyenv virtualenv 3.5.2 django-polls-test  
$ pyenv activate django-polls-test  
(django-polls-test)$ pip install --upgrade pip  
(django-polls-test)$ wget https://bitbucket.org/jhh/django-polls-app-19/raw/HEAD/requirements.txt  
(django-polls-test)$ pip install -r requirements.txt  
(django-polls-test)$ django-admin startproject mysite
```
Your directory will now look like this.

```
.
├── mysite
│   ├── manage.py
│   └── mysite
│       ├── __init__.py
│       ├── settings.py
│       ├── urls.py
│       └── wsgi.py
└── requirements.txt
```
## Settings.py ##

Modify _mysite/mysite/settings.py_.  
Add "polls" to your INSTALLED_APPS settings.  
Update TIMEZONE to "Europe/Oslo".  
```
INSTALLED_APPS = [
    ...
    'polls',
]
```

## urls.py ##

Modify _mysite/mysite/urls.py_.  
Add polls to the URLconf.
```
url(r'^polls/', include('polls.urls')),
```
Make sure you also have included "include", e.g. like this:
```
from django.conf.urls import url, include
```

## Final steps ##

Run migrate, configure a superuser and start server.
```
(django-polls-test)$ python mysite/manage.py migrate
(django-polls-test)$ python mysite/manage.py createsuperuser
(django-polls-test)$ python mysite/manage.py runserver
```

Visit admin and polls app in your web browser.  
http://127.0.0.1:8000/admin  
http://127.0.0.1:8000/polls

## Further reading ##

https://docs.djangoproject.com/en/1.9/intro/reusable-apps/  
https://pip.readthedocs.io/en/stable/reference/pip_install/  